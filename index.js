//!wrt $BSPEC:{"frn":"AaronOS Tools for Windows 96","dsc":"Run Iframe apps built for AaronOS right on Windows 96!","ssy":"GUI","cpr":"GNU General Public License 3.0","aut":"themirrazz"}

class AOSTools extends WApplication {
    constructor() { super() }
    async main(argv) {
        var url=argv[1];
        var enterURLHTMLSource=`<!DOCTYPE html>
<html>
    <head>
        <title>Enter URL</title>
        <style>
        html,body {
            margin:0;
            width:100%;
            height:100%;
            font-family: Arial,Helvetica,sans;
        }
        .flex {
            display: flex;
            flex-direction: column;
            width: 100%;
            height:100%;
            justify-content: center;
            align-items: center;
        }
        .container {
            width: 100%;
        }
        </style>
    </head>
    <body>
        <div class="flex">
            <div class="container">
                <p style="font-size:24px;margin:0;">Enter a Website URL</p>
                <div style="width: 92%;display:flex;flex-direction:row;">
                    <input onkeydown="if(event.keyCode==13){location.href=this.value}" style="width:100%;outline:none;border: 1px solid black;border-radius:3px;font-size:16px;color:black" /> <button onclick="location.href=document.querySelector('input').value" style="padding: 3px;font-size:10px;color:white;outline:none;border:none;background:green;">Go</button>
                </div>
            </div>
        </div>
    </body>`;
        if(!url) {
            url=URL.createObjectURL(
                new Blob([enterURLHTMLSource],{
                    type:"text/html"
                })
            )
        }
        var wnd=this.createWindow({
            title: "AaronOS Tools",
            body: `<iframe class="aos-tools-frame" style="border:none;outline:none;width:100%;height:100%;"></iframe>`,
            initialWidth: 800,
            initialHeight: 430,
            taskbar: true,
            center: true
        },true);
        wnd.show();
        var frame=wnd.wndObject.querySelector("iframe.aos-tools-frame");
        frame.src=url;
    }
}

return await WApplication.execAsync(new AOSTools(),this.boxedEnv.args,this);
